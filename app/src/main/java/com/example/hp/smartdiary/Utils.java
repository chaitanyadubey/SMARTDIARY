package com.example.hp.smartdiary;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

/**
 * Created by DELL on 3/23/2018.
 */

public class Utils {


    public static String TAG = "B=B Utils";

    public static final int BLE_DEVICE_SCAN_SECONDS = 10;

    public static final String KEY_USER_NAME = "KEY_USER_NAME";

    public static final String KEY_PASSWORD = "KEY_PASSWORD";

    public static final String KEY_REMEMBER_PASSWD= "KEY_REMEMBER_PASSWD";

    public static final String PREFS_NAME = "SHARED_PREFERENCE";


    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);

        return bd.doubleValue();
    }


    public static boolean isEmptyOrNull(String str) {
        boolean isEmptyOrNull = false;

        if (str == null || str.trim().length() == 0) {
            isEmptyOrNull = true;
        }

        return isEmptyOrNull;
    }





    public static SharedPreferences getSharedPreferences(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_MULTI_PROCESS );
        return sharedPreferences;

    }
    public static void setSharedPreferences(Context context, String key, String value) {

        Log.d(TAG,"setSharedPreferences");
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key,value);
        editor.commit();
    }
    public static String getSharedPreferences(Context context, String key, String defaultValue) {

        Log.d(TAG,"getSharedPreferences");
        SharedPreferences sharedPreferences = getSharedPreferences(context);

        String value = defaultValue;

        if(key !=null)
        {
            value=  sharedPreferences.getString(key, defaultValue);
        }

        return value;
    }

}
