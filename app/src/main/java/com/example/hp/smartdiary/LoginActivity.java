package com.example.hp.smartdiary;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private static EditText username;
    private static EditText password;
    private static Button btn;

    CheckBox checkBox_remember_passwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        MysqlCon db = new MysqlCon();

        db.printLogin();

        username = (EditText)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.paaword);


        checkBox_remember_passwd = (CheckBox)findViewById(R.id.checkBox_remember_passwd);

        btn = (Button)findViewById(R.id.login);

        //When the user first logs in , then extract the values of username and password froom PREFERENES and set those values on to the screen.

        // if the login is successful then save the login and password.
       String userName =  Utils.getSharedPreferences(LoginActivity.this,Utils.KEY_USER_NAME,null);
       String passwd =  Utils.getSharedPreferences(LoginActivity.this,Utils.KEY_PASSWORD,null);

       String rememberPassed = Utils.getSharedPreferences(LoginActivity.this,Utils.KEY_REMEMBER_PASSWD,null);

       if("Y".equalsIgnoreCase(rememberPassed))
       {
           checkBox_remember_passwd.setChecked(true);
       }
       else
       {
           checkBox_remember_passwd.setChecked(false);
       }

        username.setText(userName);
        password.setText(passwd);

        LOginButton();

    }

    public void LOginButton(){

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userName = username.getText().toString();
                String passwd = password.getText().toString();

                String rememberPasswd = "Y";

                if (userName.equals("birbal")&& passwd.equals("123456"))
               {

                   if(checkBox_remember_passwd.isChecked()==false)
                   {
                       userName = null;
                       passwd = null;
                       rememberPasswd = "N";
                   }

                   // if the login is successful then save the login and password.
                   Utils.setSharedPreferences(LoginActivity.this,Utils.KEY_USER_NAME,userName);
                   Utils.setSharedPreferences(LoginActivity.this,Utils.KEY_PASSWORD,passwd);
                   Utils.setSharedPreferences(LoginActivity.this,Utils.KEY_REMEMBER_PASSWD,rememberPasswd);



                    Toast.makeText(LoginActivity.this,"Login and password is correct",Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(LoginActivity.this,Smart_Diary.class);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(LoginActivity.this,"Login and password is not correct",Toast.LENGTH_SHORT).show();

                }
            }
        });
    }
}
