package com.example.hp.smartdiary;

import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;

/**
 * Created by hp on 4/15/2018.
 */

public class AttandanceFragment extends Fragment {

    View myview;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        myview = inflater.inflate(R.layout.attendance_layout,container, false);

// Student 1
        ProgressBar progressBar1 = (ProgressBar) myview.findViewById(R.id.progressBar1);
        setPercent(progressBar1,50);


// Student 2
        ProgressBar progressBar2 = (ProgressBar) myview.findViewById(R.id.progressBar2);
        setPercent(progressBar2,75);

        // Student 3
        ProgressBar progressBar3 = (ProgressBar) myview.findViewById(R.id.progressBar3);
        setPercent(progressBar3,33);


        return myview;
    }

    public void setPercent(ProgressBar progressBar,int percent)
    {
        progressBar.setProgress(percent);

        ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", 0, percent); // see this max value coming back here, we animate towards that value
        animation.setDuration(1000); // in milliseconds
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
    }
}
