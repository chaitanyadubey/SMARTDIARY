package com.example.hp.smartdiary;

import android.util.Log;

import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.lang.ClassNotFoundException;
import java.sql.SQLException.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;


/**
 * Created by hp on 4/21/2018.
 */

public class MysqlCon {

    private String TAG  = "B=B"+this.getClass();
    public void printLogin()
    {
        Log.d(TAG,"printLogin.START");

        try
        {
            // create our mysql database connection
            String myDriver = "com.mysql.jdbc.Driver";
            String myUrl = "jdbc:mysql://127.0.0.1:3306/collage";
            Class.forName(myDriver);
            Connection conn = DriverManager.getConnection(myUrl, "root", "");
            int c_id=1;
            // our SQL SELECT query.
            // if you only need a few columns, specify them by name instead of using "*"
            String query = "SELECT ENROLLMENT_NO,PASSWORD FROM LOGIN";

            // create the java statement
            Statement st = conn.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next())
            {
                String enrollmentNo = rs.getString("ENROLLMENT_NO");
                String password = rs.getString("PASSWORD");

                // print the results
                System.out.format("%s, %s\n", enrollmentNo, password);

                Log.d(TAG,"enrollmentNo="+enrollmentNo+" , password="+password);

            }
            st.close();
        }
        catch (Exception e)
        {
            Log.d(TAG,"Got an exception! ");
            Log.d(TAG,e.getMessage());
        }


        Log.d(TAG,"printLogin.ENDS");
    }

}
